'use strict'
var jwt = require('jwt-simple');
var moment = require('moment');
var secret = 'clave-secreta-para-generar-el-token-9999';

exports.authenticated =function (requiredRoles) {
    return function (req, res, next) {
        //comprobar si llega autorizacion
        if (!req.headers.authorization) {
            return res.status(404).send({message: 'La peticion no tiene la cabecera de autorization'});
        }
        //limpiar token y quitar comillas
        var tokenWithBearer = req.headers.authorization.replace(/['"]+/g, '');
        var token = tokenWithBearer.substring(7);
        //decodificar token
        try {
            var payload = jwt.decode(token, secret);
            //comprobar  si el token ha expirado
            if (payload.exp <= moment.unix()) {
                return res.status(404).send({message: 'El token ha expirado'});
            }
            if(requiredRoles.length > 0){
                const userRoles = payload.role;
                const hasRequiredRole = requiredRoles.some(role => userRoles === role);
                if (!hasRequiredRole) {
                    return res.status(403).send({ message: 'No tienes permisos para acceder a esta ruta' });
                }
            }
        } catch (ex) {
            console.log(ex);
            return res.status(403).send({message: 'El token no es valido'});
        }
        //adjuntar usuario identificado a la request
        req.user = payload;
        //pasar a la accion
        next();
    };
};