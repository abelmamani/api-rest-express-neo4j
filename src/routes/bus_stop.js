var express = require("express");
var router = express.Router();
var busStopController = require("../controllers/bus_stop"); 
var md_auth = require('../middelware/auth');

router.post("/bus_stop", md_auth.authenticated(['ROLE_ADMIN']), busStopController.createBusStop);
router.get("/bus_stop", md_auth.authenticated(['ROLE_ADMIN', 'ROLE_USER']), busStopController.getBusStops);
router.get("/bus_stop/:id", md_auth.authenticated([]), busStopController.getBusStop);
router.put("/bus_stop/:id", md_auth.authenticated([]), busStopController.updateBusStop);
router.delete("/bus_stop/:id", md_auth.authenticated([]), busStopController.deleteBusStop);
module.exports = router;