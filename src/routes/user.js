var express = require("express");
var router = express.Router();
var userController = require("../controllers/user"); 

router.post("/user/register", userController.save);
router.post("/user/login", userController.login);
router.get("/user", userController.getUsers);
router.get("/user/:id", userController.getUser);

module.exports = router;