const neo4j = require('neo4j-driver');
require('dotenv').config();
const driver = neo4j.driver(process.env.NEO4J_URI, neo4j.auth.basic(process.env.NEO4J_USERNAME, process.env.NEO4J_PASSWORD));
const userService = {
  getUsers: async () => {
    const session = driver.session();
    const result = await session.run('MATCH (u:User) RETURN u');
    session.close();
    return result.records.map(record => record.get('u').properties);
  },
  getUserByEmail: async (email) => {
    const session = driver.session();
    const result = await session.run('MATCH (u:User {email: $email}) RETURN u', { email });
    session.close();
    return result.records[0] ? result.records[0].get('u').properties : null;
  },
  getUser: async (id) => {
    const session = driver.session();
    const result = await session.run('MATCH (u:User {id: $id}) RETURN u', { id });
    return result.records[0] ? result.records[0].get('u').properties : null;
  },

  createUser: async (user) => {
    const session = driver.session();
    const result = await session.run(
      'CREATE (u:User {id: $id, name: $name, surname: $lastName, email: $email, password: $password, role: $role}) RETURN u',
      {id: user.id, name: user.name, lastName: user.lastName,email: user.email, password: user.password, role: user.role});
    session.close();
    return result.records[0].get('u').properties | null;
  },
};
module.exports = userService;
