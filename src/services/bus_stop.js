// services/busStopService.js
const neo4j = require('neo4j-driver');
require('dotenv').config();
const driver = neo4j.driver(process.env.NEO4J_URI, neo4j.auth.basic(process.env.NEO4J_USERNAME, process.env.NEO4J_PASSWORD));
const session = driver.session();

var service = {
  createBusStop: async (busStop) => {
    const result = await session.run('CREATE (bs:BusStop {name: $name, latitude: $latitude, longitude: $longitude}) RETURN bs',
      { name: busStop.name, latitude: busStop.latitude, longitude: busStop.longitude });

    return result.records[0].get('bs').properties;
  },

  getBusStops: async () => {
    const result = await session.run('MATCH (bs:BusStop) RETURN bs');
    return result.records.map(record => record.get('bs').properties);
  },

  getBusStop: async (id) => {
    const result = await session.run('MATCH (bs:BusStop) WHERE id(bs) = toInteger($id) RETURN bs', { id });
    return result.records[0] ? result.records[0].get('bs').properties : null;
  },
  getBusStopByName: async (name) => {
    const result = await session.run('MATCH (bs:BusStop {name: $name}) RETURN bs', { name });
    return result.records[0] ? result.records[0].get('bs').properties : null;
  },
  updateBusStop: async (id, updatedBusStop) => {
    const result = await session.run('MATCH (bs:BusStop) WHERE id(bs) = toInteger($id) SET bs += $updatedBusStop RETURN bs',
      { id, updatedBusStop });

    return result.records[0] ? result.records[0].get('bs').properties : null;
  },

  deleteBusStop: async (id) => {
    const result = await session.run('MATCH (bs:BusStop) WHERE id(bs) = toInteger($id) DELETE bs', { id });
    return result.summary.counters._stats.nodesDeleted > 0;
  },
};

module.exports = service;
