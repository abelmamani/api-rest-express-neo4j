'use strict';
var validator = require('validator');
var bcrypt = require('bcrypt');
var jwt = require('../services/jwt');
const uuid = require('uuid');
var userService = require('../services/user');

var controller = {
  save: async function (req, res) {
    var params = req.body;
    try {
      var validate_name = !validator.isEmpty(params.name);
      var validate_lastName = !validator.isEmpty(params.lastName);
      var validate_email = !validator.isEmpty(params.email) && validator.isEmail(params.email);
      var validate_password = !validator.isEmpty(params.password);
    } catch (err) {
      return res.status(200).send({ message: 'Faltan datos por enviar' });
    }

    if (validate_name && validate_lastName && validate_email && validate_password) {
      try {
        // Comprobar si el usuario existe
        var issetUser = await userService.getUserByEmail(params.email);
        if (!issetUser) {
          const hashPassword = await bcrypt.hash(params.password, 10);

          const newUser = {
            id: uuid.v4(),
            name: params.name,
            lastName: params.lastName,
            email: params.email.toLowerCase(),
            password: hashPassword,
            role: 'ROLE_USER',
          };
          var user = await userService.createUser(newUser);
          return res.status(201).send({
            message: 'Usuario creado exitosamente.',
            identity: { username: newUser.email, role: newUser.role },
            token: jwt.createToken(newUser),
          });
        } else {
          return res.status(200).send({ message: 'El usuario ya está registrado' });
        }
      } catch (error) {
        return res.status(500).json({ error: 'Error al intentar crear usuario.' });
      }
    } else {
      return res.status(200).send({ message: 'Validación de los datos de usuario incorrecta, inténtelo de nuevo' });
    }
  },
  login: async function (req, res) {
    var params = req.body;
    try {
      var validate_email = !validator.isEmpty(params.email) && validator.isEmail(params.email);
      var validate_password = !validator.isEmpty(params.password);
    } catch (err) {
      return res.status(200).send({ message: 'Faltan datos por enviar' });
    }

    if (!validate_email || !validate_password) {
      return res.status(200).send({
        message: 'Los datos son incorrectos, envíalos bien',
      });
    }
    try {
      const user = await userService.getUserByEmail(params.email.toLowerCase());
      if (user) {
        const check = await bcrypt.compare(params.password, user.password);

        if (check) {
          return res.status(200).send({
            identity: { username: user.email, role: user.role },
            token: jwt.createToken(user),
          });
        } else {
          return res.status(200).send({
            message: 'Las credenciales no son correctas',
          });
        }
      } else {
        return res.status(404).send({ message: 'El usuario no existe' });
      }
    } catch (err) {
      return res.status(500).send({
        message: 'Error al intentar identificarse',
      });
    }
  },
  getUsers: async function (req, res) {
    try {
      const users = await userService.getUsers(); // Cambia el nombre de la función
      res.json(users); // Cambia el nombre de la variable
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Internal Server Error' });
    }
  },
  getUser: async function (req, res) {
    try {
      const user = await userService.getUser(req.params.id); // Cambia el nombre de la función
      if (user) {
        res.json(user); // Cambia el nombre de la variable
      } else {
        res.status(404).json({ error: 'User not found' }); // Cambia el mensaje de error
      }
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Internal Server Error' });
    }
  },
};

module.exports = controller;
