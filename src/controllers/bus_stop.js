'use strict';
const validator = require('validator');
const busStopService = require('../services/bus_stop'); // Asegúrate de que el nombre del servicio sea correcto

var controller = {
  createBusStop: async (req, res) => {
    var params = req.body;
    try {
      var validate_name = !validator.isEmpty(params.name);
      var validate_latitude = !validator.isEmpty(params.latitude);
      var validate_longitude = !validator.isEmpty(params.longitude);
    } catch (err) {
      return res.status(400).json({ error: 'Faltan datos por enviar.' });
    }
    if (validate_name && validate_latitude && validate_longitude) {
      try {
          const busStopFound = await busStopService.getBusStopByName(params.name);

          if (!busStopFound) {
              const busStop = await busStopService.createBusStop(params);
              res.status(201).json({ message: 'BusStop creado exitosamente.', busStop });
          } else {
              return res.status(400).json({ error: 'El BusStop con el nombre ' + params.name + ' ya existe.' });
          }
      } catch (error) {
          return res.status(500).json({ error: 'Error al intentar crear BusStop.' });
      }
  } else {
      return res.status(400).json({ error: 'Validación de los datos de BusStop incorrecta. Intenta de nuevo.' });
  }
  },

  getBusStops: async (req, res) => {
    try {
      const busStops = await busStopService.getBusStops(); // Cambia el nombre de la función
      res.json(busStops); // Cambia el nombre de la variable
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Internal Server Error' });
    }
  },

  getBusStop: async (req, res) => {
    try {
      const busStop = await busStopService.getBusStop(req.params.id); // Cambia el nombre de la función
      if (busStop) {
        res.json(busStop); // Cambia el nombre de la variable
      } else {
        res.status(404).json({ error: 'BusStop not found' }); // Cambia el mensaje de error
      }
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Internal Server Error' });
    }
  },

  updateBusStop: async (req, res) => {
    var params = req.body;
    var busStopId = req.params.id;

    try {
        var validate_name = !validator.isEmpty(params.name);
        var validate_latitude = !validator.isEmpty(params.latitude);
        var validate_longitude = !validator.isEmpty(params.longitude);
    } catch (err) {
        return res.status(400).json({ error: 'Faltan datos por enviar.' });
    }

    if (validate_name && validate_latitude && validate_longitude) {
        try {
          const existingBusStop = await busStopService.getBusStop(busStopId);
          if (existingBusStop) {
            const busStopFound = await busStopService.getBusStopByName(params.name);
            if(!busStopFound){
              const updatedBusStop = await busStopService.updateBusStop(busStopId, params);
              res.status(200).json({ message: 'BusStop actualizado exitosamente.', updatedBusStop });
            }else{
              return res.status(400).json({ error: 'El nombre del bus stop ya esta en uso!' });
            }
          } else {
            return res.status(400).json({ error: 'El bus stop a actualizar no existe!' });
          }
        } catch (error) {
            return res.status(500).json({ error: 'Error al intentar actualizar BusStop.' });
        }
    } else {
        return res.status(400).json({ error: 'Validación de los datos de BusStop incorrecta. Intenta de nuevo.' });
    }
},

  deleteBusStop: async (req, res) => {
    try {
      const result = await busStopService.deleteBusStop(req.params.id); // Cambia el nombre de la función
      if (result) {
        res.json({ message: 'BusStop deleted successfully' }); // Cambia el mensaje
      } else {
        res.status(404).json({ error: 'BusStop not found' }); // Cambia el mensaje de error
      }
    } catch (error) {
      console.error(error);
      res.status(500).json({ error: 'Internal Server Error' });
    }
  }
};

module.exports = controller;
