var express = require("express");
const bodyParser = require('body-parser');
var app = express();
var bus_stop = require('./src/routes/bus_stop');
var user = require('./src/routes/user');
app.use(bodyParser.json());
app.use('/api', bus_stop);
app.use('/api', user);
app.listen(3000, function () {
  console.log("Aplicación ejemplo, escuchando el puerto 3000!");
});